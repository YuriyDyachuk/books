@extends('admin.layout')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Пользователи</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin">Главная</a></li>
                            <li class="breadcrumb-item"><a href="{{route('users.index')}}">Пользователи</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('users.edit', $user->id) }}">{{ $user->name }}</a></li>
                        </ol>
                    </div><!-- /.col -->
                    <!-- errors -->
                    <div class="col-md-12">
                        @include('admin.errors')
                    </div>
                    <!-- /.errors -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Main content -->
                <section class="content">
                {{ Form::open(['route' => ['users.update', $user->id], 'method' => 'put', 'files' => true]) }}
                <!-- Default box -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Редактируем пользователя</h3>
                        </div>
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Имя</label>
                                    <input type="text" name="name" class="form-control" id="exampleInputEmail1"
                                           value="{{ $user->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">E-mail</label>
                                    <input type="text" name="email" class="form-control" id="exampleInputEmail1"
                                           value="{{ $user->email }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Пароль</label>
                                    <input type="password" name="password" class="form-control" id="exampleInputEmail1">
                                </div>
                                <fieldset disabled>
                                <div class="form-group has-feedback">
                                    <label for="disabledSelect">Роль</label>
                                    <select name="role" id="role" class="form-control ">
                                        <option value="1" @php if( $user->is_admin == 0) echo ' selected'
                                        @endphp>Пользователь</option>
                                        <option value="2" @php if( $user->is_admin == 1) echo ' selected'
                                        @endphp>Администратор</option>
                                    </select>
                                </div>
                                </fieldset>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{ route('users.index') }}" class="btn btn-default">Назад</a>
                            <input type="hidden" name="id" value="{{$user->id}}">
                            <button class="btn btn-warning pull-right">Изменить</button>
                        </div>
                        <!-- /.box-footer-->
                    </div>
                    <!-- /.box -->
                    {{ Form::close() }}
                </section>
                <!-- /.content -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

