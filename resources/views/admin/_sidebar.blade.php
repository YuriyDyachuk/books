<ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
    <li class="treeview">
        <a href="/admin">
            <i class="fa fa-dashboard"></i> <span>Админ-панель</span>
        </a>
    </li>
    <li><a href="{{ route('books.index') }}"><i class="fa fa-book"></i> <span>Книги</span></a></li>
    <li><a href="{{ route('authors.index') }}"><i class="fa fa-pencil-square-o"></i> <span>Авторы</span></a></li>
    <li><a href="{{ route('users.index') }}"><i class="fa fa-users"></i> <span>Пользователи</span></a></li>
</ul>
