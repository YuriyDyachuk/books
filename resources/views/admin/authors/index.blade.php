@extends('admin.layout')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Авторы</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin">Главная</a></li>
                            <li class="breadcrumb-item"><a href="/admin/authors">Авторы</a></li>
                        </ol>
                    </div><!-- /.col -->
                    <!-- errors -->
                    <div class="col-md-12">
                        @include('admin.errors')
                    </div>
                    <!-- /.errors -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Листинг сущности</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class='form-group @if(count($authors)!= 0) show @else hidden @endif' id='articles-wrap'>
                    </div>
                    <div class="form-group">
                        <button type="button" name="create_record" id="create_record" class="btn btn-success btn-sm">
                            <i class="glyphicon glyphicon-plus"></i>
                        </button>
                    </div>
                    <table id="authors_table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Фамилия</th>
                            <th>Имя</th>
                            <th>Отчество</th>
                            <th>Книги автора</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $num = 1; ?>
                        @foreach($authors as $key => $author)
                            <tr class="post{{ $author->id }}">
                                <td>{{ $num++ }}</td>
                                <td>{{ $author->last_name }}</td>
                                <td>{{ $author->full_name }}</td>
                                <td>{{ $author->patronymic }}</td>
                                <td>{{ $author->getBooks() }}</td>
                                <td>
                                    <a href="{{route('authors.edit', $author->id)}}" data-id="{{$author->id}}" class="edit-modal btn btn-warning btn-sm">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </a>
                                    {{Form::open(['route'=>['authors.destroy', $author->id], 'method'=>'delete'])}}
                                    <a class="delete-modal btn btn-danger btn-sm" data-id="{{$author->id}}">
                                        <button onclick="return confirm('Вы точно хотите удалить данного автора?')" type="submit" class="delete">
                                            <i class="glyphicon glyphicon-trash"></i>
                                        </button>
                                    </a>
                                    {{Form::close()}}
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                    <br>
                    <div class="row">
                        <div class="alert alert-warning col-md-10 col-md-push-1 @if(count($authors) != 0) hidden @else show @endif" role="alert">
                            Записей нет
                            <button type="button" class="close" aria-label="Close" data-dismiss="alert">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- form Create Book -->
    <div id="formModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title-author">Создание нового автора</h4>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label class="control-label col-md-4">Фамилия</label>
                                <div class="col-md-8">
                                    <input type="text" name="last_name" id="last_name" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label class="control-label col-md-4">Имя</label>
                                <div class="col-md-8">
                                    <input type="text" name="full_name" id="full_name" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label class="control-label col-md-4">Отчество</label>
                                <div class="col-md-8">
                                    <input type="text" name="patronymic" id="patronymic" class="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label class="control-label col-md-4">Привязать книгу(и) к автору</label>
                                <div class="col-md-8">
                                    {!! Form::select('tags[]',
                                    $tags,
                                    null,
                                    ['class' =>'select2',
                                    'id' => 'tags',
                                    'multiple' => 'multiple',
                                    'data-placeholder' => 'Выберите книги']);
                                    !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group" align="center">
                            <input type="hidden" name="action" id="action" value="Add" />
                            <input type="hidden" name="hidden_id" id="hidden_id" />
                            <input type="submit" name="action_button" id="action_button" class="btn btn-warning" value="Add" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


