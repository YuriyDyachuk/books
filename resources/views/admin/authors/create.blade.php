@extends('admin.layout')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Авторы</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin">Главная</a></li>
                            <li class="breadcrumb-item"><a href="/admin/authors">Авторы</a></li>
                        </ol>
                    </div><!-- /.col -->
                    <!-- errors -->
                    <div class="col-md-12">
                        @include('admin.errors')
                    </div>
                    <!-- /.errors -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
        {!! Form::open(['route' => 'authors.store']) !!}
        <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Добавляем статью</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Фамилия</label>
                            <input type="text" name="last_name" class="form-control" id="exampleInputEmail1"
                                   value="@if( old('last_name')) {{ old('last_name') }} @else @endif">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Имя</label>
                            <input type="text" name="full_name" class="form-control" id="exampleInputEmail1"
                                   value="@if( old('full_name')) {{ old('full_name') }} @else @endif">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Отчество</label>
                            <input type="text" name="patronymic" class="form-control" id="exampleInputEmail1"
                                   value="@if( old('patronymic')) {{ old('patronymic') }} @else @endif">
                        </div>
                        <div class="form-group">
                            <label>Привязать книгу(и) к автору</label>
                            {!! Form::select('tags[]',
                                    $tags,
                                        null,
                                    ['class' =>'form-control select2',
                                    'multiple' => 'multiple',
                                    'data-placeholder' => 'Выберите книги']);
                                !!}
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ route('authors.index') }}" class="btn btn-default">Назад</a>
                    <button type="submit" class="btn btn-success pull-right">Добавить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {!! Form::close() !!}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection



