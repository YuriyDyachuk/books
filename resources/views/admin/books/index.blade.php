@extends('admin.layout')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Книги</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin">Главная</a></li>
                            <li class="breadcrumb-item"><a href="/admin/books">Книги</a></li>
                        </ol>
                    </div><!-- /.col -->
                    <!-- errors -->
                    <div class="col-md-12">
                        @include('admin.errors')
                    </div>
                    <!-- /.errors -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Листинг сущности</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class='form-group @if(count($books)!= 0) show @else hidden @endif' id='articles-wrap'>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                                <div class="pull-left">
                                    <button type="button" name="create_record" id="create_record" class="btn btn-success btn-sm">
                                        <i class="glyphicon glyphicon-plus"></i>
                                    </button>
                                </div>
                                <div class="pull-right">
                                    <form action="{{ route('books.index') }}" method="GET" class="form-inline">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="s" placeholder="Поиск">
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Search</button>
                                        </div>
                                    </form>
                                </div>
                        </div>
                    </div>
                    <br>
                    <table id="user_table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Название</th>
                                <th>Описание</th>
                                <th>Картинка</th>
                                <th>Авторы</th>
                                <th>Дата публикации</th>
                                <th>Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $num = 1;  ?>
                        @foreach($books as $key => $book)
                            <tr class="post{{ $book->id }}">
                                <td>{{ $num++ }}</td>
                                <td>{{ $book->title }}</td>
                                <td>{{ $book->description }}</td>
                                <td>
                                    <img src="{{ $book->getImage() }}" alt="" width="100">
                                </td>
                                <td>{{ $book->getAuthors() }}</td>
                                <td>{{ $book->publish }}</td>
                                <td>
                                    <a href="{{ route('books.edit', $book->id) }}" data-id="{{$book->id}}" class="edit-modal btn btn-warning btn-sm">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </a>
                                    {{Form::open(['route'=>['books.destroy', $book->id], 'method'=>'delete'])}}
                                    <a class="delete-modal btn btn-danger btn-sm" data-id="{{$book->id}}">
                                        <button onclick="return confirm('Вы точно хотите удалить данную книгу?')" type="submit" class="delete">
                                            <i class="glyphicon glyphicon-trash"></i>
                                        </button>
                                    </a>
                                    {{Form::close()}}
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
                    <br>
                    <div class="row">
                        <div class="alert alert-warning col-md-10 col-md-push-1 @if(count($books) != 0) hidden @else show @endif" role="alert">
                             Записей нет
                            <button type="button" class="close" aria-label="Close" data-dismiss="alert">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- form Create Book -->
    <div id="formModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Создание новой книги</h4>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label class="control-label col-md-4">Название</label>
                                <div class="col-md-8">
                                    <input type="text" name="title" id="title" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label class="control-label col-md-4">Описание</label>
                                <div class="col-md-8">
                                    <input type="text" name="description" id="description" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label class="control-label col-md-4">Обложка книги</label>
                                <div class="col-md-8">
                                    <input type="file" name="image" id="image" class="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label class="control-label col-md-4">Привязать автора(ов) к книге</label>
                                <div class="col-md-8">
                                    {!! Form::select('tags[]',
                                    $tags,
                                    null,
                                    ['class' =>'select2',
                                    'id' => 'tags',
                                    'multiple' => 'multiple',
                                    'data-placeholder' => 'Выберите теги']);
                                    !!}
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label class="control-label col-md-4">Дата публикации</label>
                                <div class="col-md-8">
                                    <input type="date" name="publish" id="publish" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group" align="center">
                            <input type="hidden" name="action" id="action" value="Add" />
                            <input type="hidden" name="hidden_id" id="hidden_id" />
                            <input type="submit" name="action_button" id="action_button" class="btn btn-warning" value="Add" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


