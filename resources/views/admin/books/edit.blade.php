@extends('admin.layout')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Книги</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin">Главная</a></li>
                            <li class="breadcrumb-item"><a href="{{route('books.index')}}">Книги</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('books.edit', $book->id) }}">{{ $book->title }}</a></li>
                        </ol>
                    </div><!-- /.col -->
                    <!-- errors -->
                    <div class="col-md-12">
                        @include('admin.errors')
                    </div>
                    <!-- /.errors -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
        {{ Form::open(['route' => ['books.update', $book->id], 'files' => true, 'method' => 'put']) }}
        <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Обновляем книгу</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Название</label>
                            <input type="text" name="title" class="form-control" id="exampleInputEmail1"
                                   value="{{$book->title}}">
                        </div>
                        <div class="form-group">
                            <img src="{{ $book->getImage() }}" alt="" class="img-responsive" width="200">
                            <label for="exampleInputFile">Лицевая картинка</label>
                            <input type="file" name="image" id="exampleInputFile">

                            <p class="help-block">Какое-нибудь уведомление о форматах..</p>
                        </div>
                        <div class="form-group">
                            <label>Авторы</label>
                            {!! Form::select('tags[]',
                                $tags,
                                $selectedAuthors,
                                ['class' =>'form-control select2',
                                'multiple' => 'multiple',
                                'data-placeholder' => 'Выберите авторов']);
                            !!}
                        </div>
                        <!-- Date -->
                        <div class="form-group">
                            <label>Дата публикации:</label>

                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="date" name="publish" class="form-control pull-right"
                                       value="{{ $book->publish }}">
                            </div>
                            <!-- /.input group -->
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Описание</label>
                            <textarea name="description" id="" cols="30" rows="10" class="form-control">{{ $book->description }}</textarea>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ route('books.index') }}" class="btn btn-default">Назад</a>
                    <button type="submit" class="btn btn-warning pull-right">Изменить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {!! Form::close() !!}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection


