<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminPanel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="/css/admin.css">

    <style>
        table.table form
        {
            display: inline-block;
        }
        button.delete
        {
            background: transparent;
            border: none;
            color: #337ab7;
            padding: 0;
        }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini" id="headers">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="/admin" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>LTE</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Admin</b>LTE</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ auth()->user()->name }}</p>
                    <small><i class="fa fa-circle text-success"></i> Online</small>
                    <a href="{{ route('users.edit', auth()->user()->id ) }}">Profile</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            @include('admin._sidebar')
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    @yield('content')
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <strong>Copyright &copy; 2020 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
        All rights reserved.
    </footer>

    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script src="/js/admin.js"></script>

<script>
    $(document).ready(function () {
        $('#create_record').click(function () {

            $('.modal-title').text('Добавление новой книги');
            $('#action_button').val('Add');
            $('#action').val('Add');

            $('#formModal').modal('show');
        });
        $('#sample_form').on('submit', function (e) {
            e.preventDefault();

            if ($('#action').val() == "Add")
            {
             $.ajax({
                    url:"{{ route('books.store') }}",
                    method:"POST",
                    dataSrc: "",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType:'json',
                    success:function(data)
                    {
                        let html = '';
                        if (data.errors)
                        {
                            html = '<div class="alert alert-danger">';
                            for (let count = 0; count < data.errors.length; count++)
                            {
                                html += '<p>' + data.errors[count] + '</p>';
                            }
                            html += '</div>';
                        }
                        if (data.success)
                        {
                            html = '<div class="alert alert-success">' + data.success + '</div>';
                            $('#sample_form')[0].reset();
                            // $('#user_table').DataTable.ajax.reload();
                            $('#user_table').load("{{ route('books.index') }} #user_table");
                        }
                        $('#form_result').html(html);
                    }
                })
            }
        });

        $('#title').val('');
        $('#description').val('');
        $('#image').val('');
        $('#publish').val('');
    });
</script>

<script>
    $(document).ready(function () {
        $('#create_record').click(function () {

            $('.modal-title-author').text('Добавление нового автора');
            $('#action_button').val('Add');
            $('#action').val('Add');

            $('#formModal').modal('show');
        });
        $('#sample_form').on('submit', function (e) {
            e.preventDefault();

            if ($('#action').val() == "Add")
            {
                $.ajax({
                    url:"{{ route('authors.store') }}",
                    method:"POST",
                    dataSrc: "",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType:'json',
                    success:function(data)
                    {
                        let html = '';
                        if (data.errors)
                        {
                            html = '<div class="alert alert-danger">';
                            for (let count = 0; count < data.errors.length; count++)
                            {
                                html += '<p>' + data.errors[count] + '</p>';
                            }
                            html += '</div>';
                        }
                        if (data.success)
                        {
                            html = '<div class="alert alert-success">' + data.success + '</div>';
                            $('#sample_form')[0].reset();
                            // $('#user_table').DataTable.ajax.reload();
                            $('#authors_table').load("{{ route('authors.index') }} #authors_table");
                        }
                        $('#form_result').html(html);
                    }
                })
            }
        });

        $('#last_name').val('');
        $('#full_name').val('');
        $('#patronymic').val('');
    });
</script>

</body>
</html>
