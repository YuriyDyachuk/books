$('#create_record').click(function () {

    $('.modal-title').text('Добавление нового автора');
    $('#action_button').val('Add');
    $('#action').val('Add');

    $('#formModal').modal('show');
});
$('#sample_form').on('submit', function (e) {
    e.preventDefault();

    if ($('#action').val() == "Add")
    {
        $.ajax({
            url:"{{ route('authors.store') }}",
            method:"POST",
            dataSrc: "",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType:'json',
            success:function(data)
            {
                let html = '';
                if (data.errors)
                {
                    html = '<div class="alert alert-danger">';
                    for (let count = 0; count < data.errors.length; count++)
                    {
                        html += '<p>' + data.errors[count] + '</p>';
                    }
                    html += '</div>';
                }
                if (data.success)
                {
                    html = '<div class="alert alert-success">' + data.success + '</div>';
                    $('#sample_form')[0].reset();
                    // $('#user_table').DataTable.ajax.reload();
                    $('#user_table').load("{{ route('authors.index') }} #user_table");
                }
                $('#form_result').html(html);
            }
        })
    }
});

$('#').val('');
$('#description').val('');
$('#image').val('');
$('#publish').val('');
