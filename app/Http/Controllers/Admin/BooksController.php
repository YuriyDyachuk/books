<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminUpdateBookValidation;
use App\Models\Repositories\AdminAuthorRepositoryInterface;
use App\Models\Repositories\AdminBooksRepositoryInterface;
use App\Models\Services\AdminBookService;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    /**
//     * @var AdminBooksRepository
     */
    private $adminBooksRepository;
    /**
//     * @var AdminAuthorRepository
     */
    private $adminAuthorRepository;
    /**
     * @var AdminBookService
     */
    private $adminBookService;

    /**
     * BooksController constructor.
     * @param AdminBooksRepositoryInterface $adminBooksRepository
     * @param AdminAuthorRepositoryInterface $adminAuthorRepository
     * @param AdminBookService $adminBookService
     */
    public function __construct(
        AdminBooksRepositoryInterface $adminBooksRepository,
        AdminAuthorRepositoryInterface $adminAuthorRepository,
        AdminBookService $adminBookService)
    {
        $this->adminBooksRepository = $adminBooksRepository;
        $this->adminAuthorRepository = $adminAuthorRepository;

        $this->adminBookService = $adminBookService;
    }

    public function index()
    {
        $books = $this->adminBooksRepository->all(10);
        $tags = $this->adminAuthorRepository->getAll();
        return view('admin.books.index',['books' => $books],['tags' => $tags]);
    }

    public function create()
    {
        $tags = $this->adminAuthorRepository->getAll();
        return view('admin.books.create',['tags' => $tags]);
    }

    public function store(Request $request)
    {
        return $this->adminBookService->getRequestCreateBook($request);
    }

    public function edit($id)
    {
        $book = $this->adminBooksRepository->bookFind($id);
        $tags = $this->adminAuthorRepository->getAll();
        $selectedAuthors = $this->adminBooksRepository->selectedAuthor($id);
        return view('admin.books.edit',['book' => $book,'selectedAuthors' => $selectedAuthors,'tags' => $tags]);
    }

    public function update(Request $request, $id, AdminUpdateBookValidation $adminUpdateBookValidation)
    {
        return $this->adminBookService->getRequestUpdateBook($request,$id,$adminUpdateBookValidation);
    }

    public function destroy($id)
    {
        return $this->adminBookService->getRequestDeleteBook($id);
    }
}
