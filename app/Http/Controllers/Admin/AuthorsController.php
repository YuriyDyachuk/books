<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Repositories\AdminAuthorRepositoryInterface;
use App\Models\Repositories\AdminBooksRepositoryInterface;
use App\Models\Services\AdminAuthorService;
use Illuminate\Http\Request;

class AuthorsController extends Controller
{
    /**
//     * @var AdminAuthorRepository
     */
    private $adminAuthorRepository;
    /**
//     * @var AdminBooksRepository
     */
    private $adminBooksRepository;
    /**
     * @var AdminAuthorService
     */
    private $adminAuthorService;

    /**
     * AuthorsController constructor.
     * @param AdminAuthorRepositoryInterface $adminAuthorRepository
     * @param AdminBooksRepositoryInterface $adminBooksRepository
     * @param AdminAuthorService $adminAuthorService
     */
    public function __construct(
        AdminAuthorRepositoryInterface $adminAuthorRepository,
        AdminBooksRepositoryInterface $adminBooksRepository,
        AdminAuthorService $adminAuthorService)
    {
        $this->adminAuthorRepository = $adminAuthorRepository;
        $this->adminBooksRepository = $adminBooksRepository;
        $this->adminAuthorService = $adminAuthorService;
    }

    public function index()
    {
        $authors = $this->adminAuthorRepository->all(10);
        $tags = $this->adminBooksRepository->getAll();
        return view('admin.authors.index',['authors' => $authors,'tags' => $tags]);
    }

    public function create()
    {
        $tags = $this->adminBooksRepository->getAll();
        return view('admin.authors.create',['tags' => $tags]);
    }

    public function store(Request $request)
    {
        return $this->adminAuthorService->getRequestCreateBook($request);
    }

    public function edit($id)
    {
        $author = $this->adminAuthorRepository->authorFind($id);
        $tags = $this->adminBooksRepository->getAll();
        $selectedAuthors = $this->adminAuthorRepository->selectedBook($id);
        return view('admin.authors.edit',['author' => $author,'selectedAuthors' => $selectedAuthors,'tags' => $tags]);
    }

    public function update(Request $request, $id)
    {
        return $this->adminAuthorService->getRequestUpdateAuthor($request,$id);
    }

    public function destroy($id)
    {
        return $this->adminAuthorService->getRequestDeleteAuthor($id);
    }
}
