<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminUserRequestValidation;
use App\Models\Repositories\AdminUserRepository;
use App\Models\Services\AdminUserService;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * @var AdminUserRepository
     */
    private $adminUserRepository;
    /**
     * @var AdminUserService
     */
    private $adminUserService;

    /**
     * UsersController constructor.
     * @param AdminUserRepository $adminUserRepository
     * @param AdminUserService $adminUserService
     */
    public function __construct(AdminUserRepository $adminUserRepository, AdminUserService $adminUserService)
    {
        $this->adminUserRepository = $adminUserRepository;
        $this->adminUserService = $adminUserService;
    }

    public function index()
    {
        $users = $this->adminUserRepository->all(10);
        return view('admin.users.index',['users' => $users]);
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request, AdminUserRequestValidation $adminUserRequestValidation)
    {
        return $this->adminUserService->getRequestCreateUser($request,$adminUserRequestValidation);
    }

    public function edit($id)
    {
        $user = $this->adminUserRepository->userFind($id);
        return view('admin.users.edit',['user' => $user]);
    }

    public function update(Request $request, $id, AdminUserRequestValidation $adminUserRequestValidation)
    {
        return $this->adminUserService->getRequestUpdateUser($request,$id,$adminUserRequestValidation);
    }

    public function destroy($id)
    {
        return $this->adminUserService->getRequestDeleteUserId($id);
    }
}
