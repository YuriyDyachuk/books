<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Author;
use App\Models\Book;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminDashboardController extends Controller
{
    public function index()
    {
        $author = Author::count();
        $book = Book::count();
        $user = User::count();

        return view('admin.index',['author' => $author, 'book' => $book, 'user' => $user]);
    }
}
