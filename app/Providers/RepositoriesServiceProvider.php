<?php

namespace App\Providers;

use App\Models\Repositories\AdminAuthorRepository;
use App\Models\Repositories\AdminAuthorRepositoryInterface;
use App\Models\Repositories\AdminBooksRepository;
use App\Models\Repositories\AdminBooksRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(AdminBooksRepositoryInterface::class,AdminBooksRepository::class);
        $this->app->bind(AdminAuthorRepositoryInterface::class,AdminAuthorRepository::class);
    }
}
