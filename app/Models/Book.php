<?php

namespace App\Models;

use App\Models\Traits\BooksSearchScopes;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $image
 * @property string $publish
 * @property string $created_at
 * @property string $updated_at
 * @property Author[] $authors
 * @mixin \Eloquent
 */
class Book extends Model
{
    use Sluggable, BooksSearchScopes;

    /**
     * @var string[]
     */
    protected $fillable = ['user_id','title','slug','description','image','author_id','publish'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function authors()
    {
        return $this->belongsToMany(
            Author::class,
            'author_books',
            'author_id',
            'book_id'
        );
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    // -------------

    /**
     * @param $fields
     * @return static
     * Добавление книги
     */
    public static function add($fields)
    {
        $books = new static();
        $books->fill($fields);
        $books->user_id = auth()->user()->id;
        $books->save();

        return $books;
    }

    /**
     * @param $fields
     * Редактирование книги
     */
    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    /**
     * @throws \Exception
     * Удаление книги
     */
    public function remove()
    {
        $this->removeImage();
        $this->delete();
    }

    /**
     * @param $ids
     * Сохранение авторов к книгам
     * принимаем массив из id [1,3,5,6 ....]
     */
    public function setAuthors($ids)
    {
        if ($ids == null){return;}
        $this->authors()->sync($ids);
    }

    public function removeImage()
    {
        if ($this->image != null)
        {
            Storage::delete('books/' . $this->image);
        }
    }

    public function uploadImage($image)
    {
        if ($image == null) { return; }

        $this->removeImage();
        $filename = Str::random(10) . '.' . $image->extension();
        $image->storeAs('books',$filename);
        $this->image = $filename;
        $this->save();
    }

    public function getImage()
    {
        if ($this->image == null)
        {
            return '/img/no-image.png';
        }
        return '/books/' . $this->image;
    }

    public function getAuthors()
    {
        return (!$this->authors->isEmpty())
            ?   implode(', ', $this->authors()->pluck('last_name')->all())
            : 'Авторы отсутствуют.';
    }

}
