<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $author_id
 * @property integer $book_id
 * @property string $created_at
 * @mixin \Eloquent
 */
class AuthorBook extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = ['author_id','book_id'];
}
