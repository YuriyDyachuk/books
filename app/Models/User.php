<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property integer $is_admin
 * @property string $created_at
 * @property string $updated_at
 * @property Author[] $authors
 * @property Book $books
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;

    const ADMIN = 1;
    const USER = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'is_admin'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function books()
    {
        return $this->hasMany(Book::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function authors()
    {
        return $this->hasMany(Author::class);
    }

    /**
     * Admin
     */
    public function setAdmin()
    {
        $this->is_admin = self::ADMIN;
    }

    /**
     * User
     */
    public function setUser()
    {
        $this->is_admin = self::USER;
    }

    public function toggleAdmin($value)
    {
        if ($value == null)
        {
            return $this->setUser();
        }

        return $this->setAdmin();
    }


    // -----------

    /**
     * @param $fields
     * Добавление пользователя
     */
    public static function add($fields)
    {
        $user = new static();
        $user->fill($fields);
        $user->password = bcrypt($fields['password']);
        $user->save();
    }

    /**
     * @param $fields
     * Редактирование пользователя
     */
    public function edit($fields)
    {
        $this->fill($fields);
        $this->password = bcrypt($fields['password']);
        $this->save();
    }

    /**
     * @throws \Exception
     * Удаление пользователя
     */
    public function remove()
    {
        $this->delete();
    }

}
