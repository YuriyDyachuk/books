<?php

namespace App\Models\Repositories;

use App\Models\User;
use Illuminate\Http\Request;

class AdminUserRepository
{
    public function all($perPage = null)
    {
        $fields = ['id','name','email','is_admin'];

        return User::select($fields)
            ->orderByDesc('id')
            ->paginate($perPage);
    }

    public function add($fields)
    {
        return User::add($fields);
    }

    public function userFind($id)
    {
        return User::orderBy('id')
            ->where('id',$id)
            ->with('books','authors')
            ->firstOrFail();
    }

    public function update($id)
    {
        $forms = \request()->only('name','email','password','role');
        $result = $this->userFind($id);
        $result->edit($forms);
    }

    public function destroy($id)
    {
        return User::findOrFail($id)->delete();
    }

}
