<?php

namespace App\Models\Repositories;

use App\Models\Author;
use App\Models\Book;

class AdminAuthorRepository implements AdminAuthorRepositoryInterface
{
    public function getAll()
    {
        return Author::pluck('last_name','id')
            ->all();
    }

    public function all($perPage = null)
    {
        $fields = ['id','last_name','full_name','patronymic'];

        return Author::select($fields)
            ->orderByDesc('id')
            ->paginate($perPage);
    }

    public function add($fields)
    {
        $author = Author::add($fields);
        $author->setBooks(request('tags'));
    }

    public function authorFind($id)
    {
        return Author::orderBy('id')
            ->where('id',$id)
            ->with('books')
            ->firstOrFail();
    }

    public function update($id)
    {
        $forms = \request()->only('last_name','full_name','patronymic','tags');
        $result = $this->authorFind($id);
        $result->edit($forms);
        $result->setBooks(request('tags'));
    }

    public function delete($id)
    {
        return Author::find($id)->remove();
    }

    public function selectedBook($id)
    {
        $res = $this->authorFind($id);
        return $res->books->pluck('id')->all();
    }
}
