<?php

namespace App\Models\Repositories;

interface AdminAuthorRepositoryInterface
{
    public function getAll();

    public function all($perPage = null);

    public function add($fields);

    public function authorFind($id);

    public function update($id);

    public function delete($id);

    public function selectedBook($id);
}
