<?php

namespace App\Models\Repositories;

interface AdminBooksRepositoryInterface
{
    public function getAll();

    public function all($perPage = null);

    public function add($fields);

    public function bookFind($id);

    public function update($id);

    public function delete($id);

    public function selectedAuthor($id);
}
