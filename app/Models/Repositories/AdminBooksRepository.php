<?php

namespace App\Models\Repositories;

use App\Models\Book;

class AdminBooksRepository implements AdminBooksRepositoryInterface
{
    public function getAll()
    {
        return Book::pluck('title','id')
            ->all();
    }

    public function all($perPage = null)
    {
        $s = request('s');
        return Book::with('authors')
            ->authors($s)
            ->orderByDesc('id')
            ->get();
    }

    public function add($fields)
    {
        $book = Book::add($fields);
        $book->uploadImage(request()->file('image'));
        $book->setAuthors(request('tags'));
    }

    public function bookFind($id)
    {
        return Book::orderBy('id')
            ->where('id',$id)
            ->with('authors')
            ->firstOrFail();
    }

    public function update($id)
    {
        $forms = \request()->only('title','description','image','tags','publish');
        $result = $this->bookFind($id);
        $result->edit($forms);
        $result->uploadImage(request('image'));
        $result->setAuthors(request('tags'));
    }

    public function delete($id)
    {
        return Book::find($id)->remove();
    }

    public function selectedAuthor($id)
    {
        $res = $this->bookFind($id);
        return $res->authors->pluck('id')->all();
    }

}
