<?php

namespace App\Models\Traits;

use App\Models\Book;
use Illuminate\Database\Eloquent\Builder;


trait BooksSearchScopes
{
    public function scopeAuthors(Builder $query, $s, $or = false)
    {
        if($or === true){
            $term = "%$s%";
            return $query
                ->where('title', 'like', '%'.$term.'%')
                ->orWhere('description', 'like', '%'.$term.'%');
        }else{
            $term = "%$s%";
            return $query
                ->where('title', 'like', '%'.$term.'%')
                ->orWhere('description', 'like', '%'.$term.'%');
        }
    }
}
