<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $last_name
 * @property string $full_name
 * @property string $patronymic
 * @property string $created_at
 * @property string $updated_at
 * @property Book[] $books
 * @property User[] $users
 * @mixin \Eloquent
 */
class Author extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = ['user_id','last_name','full_name','patronymic'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function books()
    {
        return $this->belongsToMany(
            Book::class,
            'author_books',
            'book_id',
            'author_id'
        );
    }

    // ----------

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function users()
    {
        return $this->hasOne(User::class);
    }


    /**
     * @param $fields
     * @return static
     * Добавление автора
     */
    public static function add($fields)
    {
        $authors = new static();
        $authors->user_id = auth()->user()->id;
        $authors->fill($fields);
        $authors->save();

        return $authors;
    }

    /**
     * @param $fields
     * Редактирование автора
     */
    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    /**
     * @throws \Exception
     * Удаление автора
     */
    public function remove()
    {
        $this->delete();
    }

    /**
     * @param $ids
     * Сохранение книг к авторам
     * принимаем массив из id [1,3,5,6 ....]
     */
    public function setBooks($ids)
    {
        if ($ids == null){return;}
        $this->books()->sync($ids);
    }

    public function getBooks()
    {
        return (!$this->books->isEmpty())
            ?   implode(', ', $this->books()->pluck('title')->all())
            : 'Книги отсутствуют.';
    }

}
