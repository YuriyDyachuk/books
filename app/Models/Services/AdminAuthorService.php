<?php

namespace App\Models\Services;

use App\Models\Repositories\AdminAuthorRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AdminAuthorService
{
    /**
     * @var AdminAuthorRepository
     */
    private $adminAuthorRepository;

    /**
     * AdminAuthorService constructor.
     * @param AdminAuthorRepository $adminAuthorRepository
     */
    public function __construct(AdminAuthorRepository $adminAuthorRepository)
    {
        $this->adminAuthorRepository = $adminAuthorRepository;
    }

    public function getRequestCreateBook(Request $request)
    {
        $forms = $request->only('last_name','full_name','patronymic','tags');
        $rules = [
            'last_name' => 'required|string',
            'full_name' => 'required|string',
        ];
        $error = Validator::make($forms,$rules);
        if ($error->fails()){
            return response()->json(['errors' => $error->errors()->all()]);
        }
        $this->adminAuthorRepository->add($forms);
        return response()->json(['success' => 'Данные успешно сохранены']);
    }

    public function getRequestUpdateAuthor(Request $request, $id)
    {
        $forms = $request->only('last_name','full_name','patronymic','tags');
        if (!$forms) {
            return back()->withErrors(['msg' => 'Ошибка обновления!'])->withInput();
        }
        $this->adminAuthorRepository->update($id);
        return back()->with(['success' => 'Автор успешно обновлен']);
    }

    public function getRequestDeleteAuthor($id)
    {
        if (!$id) {
            return back()->withErrors(['msg' => 'Ошибка удаления!'])->withInput();
        }
        $this->adminAuthorRepository->delete($id);
        return back()->with(['success' => 'Автор успешно удален']);
    }
}
