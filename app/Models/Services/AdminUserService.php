<?php

namespace App\Models\Services;

use App\Http\Requests\AdminUserRequestValidation;
use App\Models\Repositories\AdminUserRepository;
use Illuminate\Http\Request;

class AdminUserService
{
    /**
     * @var AdminUserRepository
     */
    private $adminUserRepository;

    /**
     * AdminUserService constructor.
     * @param AdminUserRepository $adminUserRepository
     */
    public function __construct(AdminUserRepository $adminUserRepository)
    {
        $this->adminUserRepository = $adminUserRepository;
    }

    public function getRequestCreateUser(Request $request, AdminUserRequestValidation $adminUserRequestValidation)
    {
        $forms = $request->only('name','email','password','new-password');
        if (empty($forms)) {
            return back()->withErrors(['msg' => 'Ошибка создания!'])->withInput();
        }
        $this->adminUserRepository->add($forms);
        return redirect()->route('users.index')->with(['success' => 'Пользователь успешно создан']);
    }

    public function getRequestUpdateUser(Request $request, $id, AdminUserRequestValidation $adminUserRequestValidation)
    {
        $forms = $request->only('name','email','password');
        if (!$forms) {
            return back()->withErrors(['msg' => 'Ошибка обновления!'])->withInput();
        }
        $this->adminUserRepository->update($id);
        return back()->with(['success' => 'Пользователь успешно обновлен']);
    }

    public function getRequestDeleteUserId($id)
    {
        if (empty($id)) {
            return back()->withErrors(['msg' => 'Ошибка удаления!'])->withInput();
        }
        $this->adminUserRepository->destroy($id);
        return back()->with(['success' => 'Пользователь успешно удален!']);
    }
}
