<?php

namespace App\Models\Services;

use App\Http\Requests\AdminUpdateBookValidation;
use App\Models\Repositories\AdminBooksRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AdminBookService
{
    /**
     * @var AdminBooksRepository
     */
    private $adminBooksRepository;

    /**
     * AdminBookService constructor.
     * @param AdminBooksRepository $adminBooksRepository
     */
    public function __construct(AdminBooksRepository $adminBooksRepository)
    {
        $this->adminBooksRepository = $adminBooksRepository;
    }

    public function getRequestCreateBook(Request $request)
    {
        $forms = $request->only('title','description','tags','image','publish');
        $rules = [
            'title' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png|max:2048',
            'publish' => 'date',
        ];
        $error = Validator::make($forms,$rules);
        if ($error->fails()){
            return response()->json(['errors' => $error->errors()->all()]);
        }
        $this->adminBooksRepository->add($forms);
        return response()->json(['success' => 'Данные успешно сохранены']);
    }

    public function getRequestUpdateBook(Request $request, $id, AdminUpdateBookValidation $adminUpdateBookValidation)
    {
        $forms = $request->only('title','description','image','tags','publish');
        if (!$forms) {
            return back()->withErrors(['msg' => 'Ошибка обновления!'])->withInput();
        }
        $this->adminBooksRepository->update($id);
        return back()->with(['success' => 'Книга успешно обновлена']);
    }

    public function getRequestDeleteBook($id)
    {
        if (!$id) {
            return back()->withErrors(['msg' => 'Ошибка удаления!'])->withInput();
        }
        $this->adminBooksRepository->delete($id);
        return back()->with(['success' => 'Книга успешно удалена']);
    }
}
