<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@example.com',
                'password' => bcrypt(123),
                'is_admin' => 1,
            ],
            [
                'id' => 2,
                'name' => 'User',
                'email' => 'user@example.com',
                'password' => bcrypt(123),
                'is_admin' => 0,
            ]
        ];
        DB::table('users')->insert($data);
    }

}
