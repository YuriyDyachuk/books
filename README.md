Documentation start project

------------------
- git clone https://gitlab.com/YuriyDyachuk/books.git

- Linux 
- nginx
- Config
------------------
    server {
        listen 80;
        listen [::]:80;
        root /var/www/html/..../public;
        index  index.php index.html index.htm;
        server_name  ....com/loc;
    
        location / {
            try_files $uri $uri/ /index.php?$query_string;
        }
    
    
        location ~ \.php$ {
           include snippets/fastcgi-php.conf;
           fastcgi_pass             unix:/var/run/php/php7.4-fpm.sock; / ваша версия php
           fastcgi_param   SCRIPT_FILENAME $document_root$fastcgi_script_name;
        }
    
    }
------------------
- Windows 
- Xampp Apache
- Config
------------------
    #<VirtualHost url ////.com:80> 
    # DocumentRoot cd path..to..you..comp\....\public
    # ServerName url ////.com
    # <Directory "cd path..to..you..comp\....\public">
    #  Options Indexes FollowSymLinks
    #  Allow from all
    #  Require all granted
    #  IndexIgnore /
    #  RewriteEngine on
    #  RewriteCond %{REQUEST_FILENAME} !-f
    #  RewriteCond %{REQUEST_FILENAME} !-d
    #  RewriteRule . index.php
    # </Directory>
    #</VirtualHost>
------------------
- Hosts file
- 127.0.0.1 you url address
- Restart xampp (apache)
- Database mysql name create:
------------------
    # CREATE DATABASE name;
    # USE name;
    # mysql -u root -p > home/name.sql;
    # enter password;
------------------
- добавить настройки в .env file скопировал с .env.example и вставить свои данные Б/Д
- cp .env.example .env
- composer install -o
- npm install 
- npm run dev
- php artisan key:generate
- php artisan migrate
- php artisan db:seed --class=UserSeeder

- Login Page Admin
    # login --- admin@example.com,
    # pass  --- 123,



- При миграциях

- use Illuminate\Support\Facades\Schema;

- public function boot()
- {
-     Schema::defaultStringLength(191);
- }

